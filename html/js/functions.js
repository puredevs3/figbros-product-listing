const button = document.querySelector('.toggle-button');
const hiddenItems = document.querySelectorAll('.hidden-item');
let isHidden = true;
button.addEventListener('click', () => {
    button.textContent = isHidden
        ? 'Hide items'
        : 'Load more';

    isHidden = !isHidden;
    hiddenItems.forEach(item => item.classList.toggle('hidden'));
});

const button1 = document.querySelector('.toggle-button1');
const hiddenItems1 = document.querySelectorAll('.hidden-item1');
let isHidden1 = true;
button1.addEventListener('click', () => {
    button1.textContent = isHidden1
        ? 'Hide items'
        : 'Load more';

    isHidden1 = !isHidden1;
    hiddenItems1.forEach(item1 => item1.classList.toggle('hidden1'));
});

const button2 = document.querySelector('.toggle-button2');
const hiddenItems2 = document.querySelectorAll('.hidden-item2');
let isHidden2 = true;
button2.addEventListener('click', () => {
    button2.textContent = isHidden2
        ? 'Hide items'
        : 'Load more';

    isHidden2 = !isHidden2;
    hiddenItems2.forEach(item2 => item2.classList.toggle('hidden2'));
});

var selector = '.list li';

$(selector).on('click', function(){
    $(selector).removeClass('active');
    $(this).addClass('active');
});