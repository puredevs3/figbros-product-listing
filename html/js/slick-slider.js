$('.slider-area').slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
    nextArrow: '<div class="slick-custom-arrow-right"><a href="#" ><img src="images/homepage/arrow-right.png" alt="" class="img-fluid d-block arrow-right-icon"></a></div>',
    prevArrow: '<div class="slick-custom-arrow-left"><a href="#" ><img src="images/homepage/arrow-left.png" alt="" class="img-fluid d-block arrow-left-icon"></a></div>',

});

